<?php


namespace Listery\Orm\Repositories;


use Doctrine\ORM\EntityRepository;

abstract class Repository extends EntityRepository implements RepositoryInterface
{

}